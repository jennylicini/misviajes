package com.example.misviajes;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

public class DetailFragment extends Fragment {

    /*private DetailViewModel mViewModel;*/
    private View view;
    MapView mMapView;
    private GoogleMap googleMap;
    private Triposo triposo;
    private Button anadirbtn;
    boolean existe = false;



    public static DetailFragment newInstance() {
        return new DetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);

        Intent i = getActivity().getIntent();

        if (i != null) {
             triposo = (Triposo) i.getSerializableExtra("triposo");

            if (triposo != null) {
                updateUi(triposo);
            }

            //google maps
            mMapView = (MapView) view.findViewById(R.id.mapView);
            mMapView.onCreate(savedInstanceState);

            mMapView.onResume(); // needed to get the map to display immediately

            try {
                MapsInitializer.initialize(getActivity().getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            mMapView.getMapAsync(mMap -> {
                googleMap = mMap;

                // For showing a move to my location button
                //googleMap.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                LatLng ciudad = new LatLng(triposo.getLatitude(), triposo.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(ciudad).title(triposo.getName()).snippet(triposo.getLocation_id());
                googleMap.addMarker(new MarkerOptions().position(ciudad).title(triposo.getName()).snippet(triposo.getLocation_id())).showInfoWindow();
                //googleMap.addMarker(new MarkerOptions().position(ciudad).title(triposo.getName()).snippet(triposo.getLocation_id()));


                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(ciudad).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            });

            //añadir ciudad a la lista
            anadirbtn = view.findViewById(R.id.btnanadir);

            anadirbtn.setOnClickListener(button -> {
                RadioGroup rg = (RadioGroup) view.findViewById(R.id.radioGroup);
                int selectedId = rg.getCheckedRadioButtonId();

                if (selectedId == -1)
                {
                    Toast.makeText(getContext(), "SELECIONA UNA OPCION!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    DatabaseReference base = FirebaseDatabase.getInstance("https://misviajes-14dd1-default-rtdb.firebaseio.com/").getReference();

                    DatabaseReference users = base.child("users");
                    DatabaseReference uid = users.child(auth.getUid());
                    DatabaseReference misviajes = uid.child("misviajes");
                    // one of the radio buttons is checked - añadimo la ciudad a la lista
                    Triposo tr = new Triposo();

                    misviajes.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            for(DataSnapshot data: snapshot.getChildren()){
                                Triposo item = data.getValue(Triposo.class);
                                if(item.getLocation_id().equals(triposo.getLocation_id())){
                                    Toast.makeText(getContext(), "CIUDAD YA EXISTENTE EN TU LISTA", Toast.LENGTH_SHORT).show();
                                    existe = true;
                                    break;
                                }else{
                                    existe = false;
                                }
                            }
                            if(!existe){
                                tr.setLatitude(triposo.getLatitude());
                                tr.setLongitude(triposo.getLongitude());
                                tr.setName(triposo.getName());
                                tr.setLocation_id(triposo.getLocation_id());
                                tr.setImage(triposo.getImage());

                                // find the radiobutton by returned id
                                RadioButton radioButton = (RadioButton) view.findViewById(selectedId);
                                tr.setLista(radioButton.getText().toString());

                                DatabaseReference reference = misviajes.push();
                                reference.setValue(tr);

                                Toast.makeText(getContext(), "CIUDAD AÑADIDA A TU LISTA", Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                }

            });

        }

        return view;
    }





    private void updateUi(Triposo triposo) {
        Log.d("triposo", triposo.toString());

        TextView tv_location_id = view.findViewById(R.id.tv_location_id);
        TextView tv_name = view.findViewById(R.id.tv_name);
        ImageView iv_source_url = view.findViewById(R.id.iv_source_url);
        TextView tv_snippet=view.findViewById(R.id.tv_snippet);

        tv_location_id.setText(triposo.getLocation_id());
        tv_name.setText(triposo.getName());
        tv_snippet.setText(triposo.getSnippet());

        Glide.with(getContext()).load(
                triposo.getImage()
        ).into(iv_source_url);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*mViewModel = ViewModelProviders.of(this).get(DetailViewModel.class);*/
        // TODO: Use the ViewModel
    }


}
