package com.example.misviajes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;

public class CamaraActivity extends AppCompatActivity {

    private ImageView foto;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    StorageReference mStorageRef;
    private Uri imageuri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);


        foto = findViewById(R.id.foto);
        Button buttonFoto = findViewById(R.id.button_foto);
        buttonFoto.setOnClickListener(button -> {
            dispatchTakePictureIntent();
        });

        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            assert extras != null;
            imageuri = data.getData();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            foto.setImageBitmap(imageBitmap);
            byte bb[] = bytes.toByteArray();
            uploadToFirebase(bb);
        }
    }

    private void uploadToFirebase(byte[] bb) {
        java.util.Date date=new Date();
        StorageReference sr = mStorageRef.child("myimages/"+date.getTime()+"a.jpg");
        sr.putBytes(bb).addOnSuccessListener(taskSnapshot -> {
            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
            Toast.makeText(CamaraActivity.this, "Foto añadida correctamente", Toast.LENGTH_SHORT).show();
            result.addOnSuccessListener(uri -> {
                String imageUrl = uri.toString();
                addUrlToDatabase(imageUrl);
            });
        }).addOnFailureListener(e -> {
            Toast.makeText(CamaraActivity.this, "ninguna foto añadida", Toast.LENGTH_SHORT).show();
        });
    }

    private void addUrlToDatabase(String imageUrl){
        Camara cam = new Camara();
        cam.setImageurl(imageUrl);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance("https://misviajes-14dd1-default-rtdb.firebaseio.com/").getReference();

        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference misviajes = uid.child("misfotos");

        DatabaseReference reference = misviajes.push();
        reference.setValue(cam);

    }
}
