package com.example.misviajes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class TriposoAdapter extends ArrayAdapter<Triposo> {
    public TriposoAdapter(Context context, int resource, List<Triposo> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Triposo triposo = getItem(position);

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_cities_row, parent, false);
        }

        TextView tv_location_id = convertView.findViewById(R.id.tv_location_id);
        TextView tv_name = convertView.findViewById(R.id.tv_name);
        ImageView iv_source_url = convertView.findViewById(R.id.iv_source_url);

        assert triposo != null;
        tv_location_id.setText(triposo.getLocation_id());
        tv_name.setText(triposo.getName());

        Glide.with(getContext()).load(
                triposo.getImage()
        ).into(iv_source_url);

        // Retornem la View replena per a mostrar-la
        return convertView;

    }


}
