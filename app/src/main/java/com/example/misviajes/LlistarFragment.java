package com.example.misviajes;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LlistarFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_llistar, container, false);

       FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance("https://misviajes-14dd1-default-rtdb.firebaseio.com/").getReference();

        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference mv = uid.child("misviajes");


        FirebaseListOptions<Triposo> options = new FirebaseListOptions.Builder<Triposo>()
                .setQuery(mv, Triposo.class)
                .setLayout(R.layout.lv_misviajes_item)
                .setLifecycleOwner(this)
                .build();


        FirebaseListAdapter<Triposo> adapter = new FirebaseListAdapter<Triposo>(options) {
            @Override
            protected void populateView(View v, Triposo model, int position) {
                TextView tv_location_id = v.findViewById(R.id.tv_location_id2);
                TextView tv_name = v.findViewById(R.id.tv_name2);
                ImageView iv_source_url = v.findViewById(R.id.iv_source_url2);
                TextView lista = v.findViewById(R.id.tuLista);


                tv_name.setText(model.getName());
                tv_location_id.setText(model.getLocation_id());

                if (model.getLista().equals("VISITADO")){
                    lista.setBackgroundColor(0xFF4CAF50);
                }else{
                    lista.setBackgroundColor(0xFFD3BE0A);
                }

                lista.setText(model.getLista());

                Glide.with(getContext()).load(
                        model.getImage()
                ).into(iv_source_url);

            }
        };

        ListView misviajes = view.findViewById(R.id.lvMisViajes);
        misviajes.setAdapter(adapter);
        return view;
    }

}
